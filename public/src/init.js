
    var gProcessor=null;

    // Show all exceptions to the user:
    OpenJsCad.AlertUserOfUncaughtExceptions();

    function onload()
    {
      gProcessor = new OpenJsCad.Processor(document.getElementById("viewer"));
      getExample("faceShield.jscad");
      updateSolid();
    }

    function updateSolid()
    {
      gProcessor.setJsCad(document.getElementById('code').value);
    }
    function getExample(name) {
      var el = document.getElementById('code');
      $.get('printedTools/' + name, function(data) {
        el.value = data;
        updateSolid();
      });
    }
