/*
FILE   : anti_projection.scad

    DESIGN AND OPENSCAD DEFINITION : Nicolas H.-P. De Coster (Vigon) <ndcoster@meteo.be>

    OPENJSCAD DEFINITION :Adrien Centonze (Adricen) <adrien.centonze@love-open-design.com>

    PROJECT URL : https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/
    DATE   : 2020-03-20

    LICENSE : M.I.T. (https://opensource.org/licenses/MIT)

    CONTRIBUTORS :
     - Adrien Centonze (Adricen) <adrien.centonze@love-open-design.com> : scad support for pins
     - Pierre Hilson (zorlgups) <zorglups.mailing@gmail.com> : Hooks closure parameters avoid the elastic band to come out so easily

    VERSION NOTES:
        - 4.0 : OpenJscad version - fully optimized for web distribution
        - 3.4 : corrected position/dimensions for Zorglups contribution
        - 3.3 : Zorglups contribution : Enhanced hook fence generation to avoid hook weakness + dash correction where arches meet
        - 3.2 : Adricen contribution : adding support for pins in scad (sup_adri parameter)
        - 3.1 : (printing) layer height as main parameter for stacked distance and piece height automatic rounding (better slicing/printing results with thick layers)
        - 3.0 : 2020-03-25 : automatic calculation no matter the number of pins, placing them properly on circle or temple depending on fix_dist parameter
        - 2.3 : corrected bug : proper hook support
        - 2.1 : corrected bug : no more parity issue with dash line
        - 2.0 : stackable version, automatically calculated dashet support
        - 1.1 : lowered pins (limiting support)
        - 1.0 : added central pin (was avoided previously because more complex punch in transaprent foil but asked explicitely by St Pierre Hospital, thus added)
        - 0.2 : adapted pins size and position
        - 0.1 : second draft : added pins for foil clipping
        - 0.0 : first draft : clipping system
*/
function getParameterDefinitions() {
  return [
    { name: 'diametre', caption: 'Diameter :', type: 'int', default: 95 },
    { name: 'nozzle', caption: 'Nozzle size:', type: 'float', default: 0.4, step: "0.05" },
    { name: 'n_stacked', caption: 'Number of stacked pieces:', type: 'int', default: 3, min: 1 },
    { name: 'fix_n', caption: 'Number of pins:', type: 'int', default: 4,  min: 3 },
    { name: 'thikness', caption: 'General thikness of the headband:', type: 'float', default: 1.2, step: "0.05"  },
    { name: 'pin_sup', type: 'choice', caption: 'Optimized supports:', values: [1, 0], captions: [ "Yes","No"], default: 1},
  ];
}
function main(params) {
  var myBand = new faceShield(params);
  return myBand.modules.faceShieldRender().setColor(1,0.27,0)
}

class faceShield {
  constructor(parameters) {
    // Assigning values
    this.values = this.constructValues(parameters);
    this.mods = this.constructMods();
    this.modules = this.constructModules();
    this.mods.simple = this.modules.structure(this.values.out_d, this.values.in_d, this.values.hook_d, this.values.height, this.values.th);
    this.mods.stacked = this.modules.structure(this.values.out_d, this.values.in_d, this.values.hook_d, this.values.height, this.values.th, true);
  }

  constructValues(parameters) {
    return {
      e : 0.01,
      dash_l : 0.6,
      dash_int : 4,
      approx_h : 8,
      sec : 5,
      hook_d : 4,
      hook_fence : 0.6,
      fix_dist : 297 - 2*10,
      fix_d : 3,
      fix_h : 3.5,
      fix_over : 1.2,
      // From inputs
      in_d : parameters.diametre,
      n_stacked : parameters.n_stacked, //vertical repeat
      nozzle : parameters.nozzle, // Nozzle diametre
      sup_opti : parameters.pin_sup, // Support optimisation => (adricen contribution)
      th : parameters.thikness, //thickness of module (=0.4*3 or 0.6*2 for printing speed optimization)
      fix_n : parameters.fix_n, // Number of pins
      // render models
      // Getter
      get out_d() { return this.in_d+20 },
      get layer_h() { return this.nozzle/2+0.05 },
      get stacked_d() { return this.layer_h*2 },
      get height() { return Math.round(this.approx_h/this.layer_h)*this.layer_h },
      get out_r() { return this.out_d/2 },
      get in_r() { return this.in_d/2 },
      get hook_r() { return this.hook_d/2 },
      get holes_d() { return this.height/2 },
      get fix_r() { return this.fix_d/2 },
      get arc_l() { return Math.PI*(this.out_d+2*this.th)*3/4 },
      get fix_int() { return this.fix_dist/(this.fix_n-1) },
      get mid_pos() { return (this.fix_n-1)/2 },
      get alpha_int() {return 360*this.fix_int/(Math.PI*(this.out_d+2*this.th)) },
    }
  }
  constructModules(){
    var globalParent = this;

    return {
      mods: globalParent.mods,
      values: globalParent.values,
      empty_cyl(in_d, th, h, dashed=false, fn=60) {
        var empty_cylinder = new CSG();
        empty_cylinder = empty_cylinder.union(
          CSG.cylinder({start: [0, 0, 0], end: [0, 0, h], radius:in_d/2, resolution: fn})
        ).subtract(
          CSG.cylinder({start: [0, 0, 0], end: [0, 0, h+2*this.values.e], radius:in_d/2-th, resolution: fn}).translate([0,0,-this.values.e])
        )
        if(dashed){
          var circ = Math.PI*(in_d+th/2);
          var n_cube_dash = Math.ceil(circ/this.values.dash_int);
          var alpha = 360/n_cube_dash;
          for (var j = 0; j < n_cube_dash+1; j++) {
            // console.log('hopla ' + j);
            if (j%2 === 0) {
              empty_cylinder = empty_cylinder.subtract(
                // rotate([0,0,j*alpha+50], CSG.cube({radius:[(in_d/2+th+this.values.e)/2, this.values.dash_int-this.values.dash_l*th, (h+2*this.values.e)/2]}).translate([in_d/2+th,0,(h+2*this.values.e)/2-h+th])) //+30 cheating to get proper hooks support (dirty but works) -- h*1.5-th
              CSG.cube({radius:[(in_d/2+th+this.values.e)/2, this.values.dash_int-this.values.dash_l*th, (h+2*this.values.e)/2]}).translate([in_d/2+th,0,(h+2*this.values.e)/2-h+th]).rotateZ(j*alpha+50) //+30 cheating to get proper hooks support (dirty but works) -- h*1.5-th
              );
            }
          }
        }
        return empty_cylinder;
      },
      temple(l,w,h,dashed=false) {
        var escape_temple = new CSG();
        escape_temple = escape_temple.union(
          CSG.cube({radius:[l/2+w*2,w/2,h/2]})
        );
        if(dashed){
          var n_dash = Math.ceil(l/this.values.dash_int);
          for (var p = 0; p < n_dash; p++) {
            escape_temple = escape_temple.subtract(
              CSG.cube({radius:[this.values.dash_int-this.values.dash_l, h+2*this.values.e, w+2*this.values.e]}).translate([(this.values.dash_l+this.values.dash_int*p)*2-(this.values.dash_int-this.values.dash_l)*8,-this.values.e,-h/2-this.values.e])
            );
          }
        }
        return escape_temple;
      },
      arc3_4(in_d, th, h, dashed=false, fn=60) {
        // OpenJsCad.log("Hello");
        var semi_arc = new CSG();
        var semi_arc = semi_arc.union(
          this.empty_cyl(in_d, th, h, dashed, fn)
        ).subtract(
          // translate([h*4,h*4,0],CSG.cube({radius:[in_d/3.2, in_d/3.2, h+2]}))
          CSG.cube({radius:[in_d/3.2, in_d/3.2, h+2*this.values.e]}).translate([h*4,h*4,-this.values.e])
        )
        return semi_arc;
      },
      fixing(d, h, over, isStruct) {
        var outsideParent = this
        var sph_d = d+2*over;
        // creating the fixing once and for all - the element and the render are unite
        var fixing= {

          get pinElement() {
            var pin = CSG.cylinder({start:[0, 0, 0], end:[0, 0, d*1.3], radius:d/1.7, resolution:8}).union(
              CSG.sphere({radius:sph_d/2, resolution:16}).subtract(
                CSG.cube({radius:[sph_d,sph_d,sph_d/2], center:[0, 0, 0]}).translate([0,0,-sph_d/2])
              ).translate([0,0,h-d/2])
            ).translate([0, 0, outsideParent.values.out_r-outsideParent.values.th ]).rotateX(-90).rotateZ(135)

            if ( outsideParent.values.sup_opti == true ) {
              if(isStruct == true){
                pin = CSG.cylinder({start:[0, 0, 0], end:[0, 0, d*1.3], radius:d/1.7, resolution:8}).union(
                  CSG.sphere({radius:sph_d/2, resolution:16}).subtract(
                    CSG.cube({radius:[sph_d,sph_d,sph_d/2], center:[0, 0, 0]}).translate([0,0,-sph_d/2])
                  ).translate([0,0,h-d/2])
                ).union(
                  CSG.cylinder({start:[0, 0, 0], end:[0, 0, h], radius:d/3, resolution:8}).rotateX(90).translate([0,h+d/2,h-d/4])
                ).translate([0, 0, outsideParent.values.out_r-outsideParent.values.th ]).rotateX(-90).rotateZ(135)
              } else {
                pin = CSG.cylinder({start:[0, 0, 0], end:[0, 0, d*1.3], radius:d/1.7, resolution:8}).union(
                  CSG.sphere({radius:sph_d/2, resolution:16}).subtract(
                    CSG.cube({radius:[sph_d,sph_d,sph_d/2], center:[0, 0, 0]}).translate([0,0,-sph_d/2])
                  ).translate([0,0,h-d/2])
                ).union(
                  CSG.cylinder({start:[0, 0, 0], end:[0, 0, h/2], radius:d/3, resolution:8}).rotateX(90).translate([0,h-d/4,h-d/4])
                ).union(
                  CSG.cylinder({start: [0, 0, 0],  end: [0, 0, outsideParent.values.layer_h*3],  radiusStart: outsideParent.values.th*2,  radiusEnd: outsideParent.values.th,  resolution: 16}).rotateX(90).translate([0,h-d/4,h-d/4])
                ).translate([0, 0, outsideParent.values.out_r-outsideParent.values.th ]).rotateX(-90).rotateZ(135)
              }
            }
            return pin.translate([0,0,outsideParent.values.height/2])

          },
          render : new CSG(),
        }

        // generating fixing pins aroud the headband
        for(var p = 0; p<this.values.fix_n; p++){
          var pos_n = p-this.values.mid_pos;
          var alpha = pos_n * this.values.alpha_int;
          if(alpha < -135){
            fixing.render = fixing.render.union(
              // rotate([0,0,-135],fixing.pinElement).translate([0,(this.values.fix_dist-this.values.arc_l)/2,0])
              fixing.pinElement.rotateZ(-135).translate([0,(this.values.fix_dist-this.values.arc_l)/2,0])
            )
          } else if(alpha > 135) {
            fixing.render = fixing.render.union(
              // rotate([0,0,135],fixing.pinElement).translate([0,(this.values.fix_dist-this.values.arc_l)/2,0])
              fixing.pinElement.rotateZ(135).translate([0,(this.values.fix_dist-this.values.arc_l)/2,0])
            )
          } else {
            fixing.render = fixing.render.union(
              // rotate([0,0,alpha],fixing.pinElement)
              fixing.pinElement.rotateZ(alpha)
            )
          }
          // console.log(alpha)
        }
        return fixing.render
        // return rotate([-90,0,135],fixing_up.translate([0, 0, this.values.out_r-this.values.th ])).translate([0,0,this.values.height/2]);
      },
      structure(out_d, in_d, hook_d, height, th, isStruct=false){
        var out_r = out_d/2;
        var in_r = in_d/2;
        var hook_r = hook_d/2;
        var arc_dist = 3*Math.PI*in_d/4;
        var n_holes = Math.floor(arc_dist/height);
        var output_structure = new CSG();
        var output_buffer = new CSG();

        // Arcs
        output_structure = output_structure.union(
          this.arc3_4(out_d, th, height, isStruct)
        )
        output_buffer = output_buffer.union(
          this.arc3_4(in_d, th, height, isStruct).translate([out_r-in_r, out_r-in_r, 0])
        )
        for (var k = 2; k < n_holes+1; k++) {
          output_buffer = output_buffer.subtract(
            CSG.cylinder({start: [0, 0, in_d/2-th*2-this.values.e], end: [0, 0, in_d/2+th*2+this.values.e], radius: this.values.holes_d/2, resolution:6}).rotateX(90).rotateZ(90-k*270/(n_holes+2)).translate([out_r-in_r, out_r-in_r, this.values.holes_d])
          )
        }

        output_structure = output_structure.union(output_buffer)

        // Temples
        output_structure = output_structure.union(
          this.temple(out_r+th-this.values.sec, th, height, isStruct).translate([(out_r+th-this.values.sec)/2-th*1.5, out_r-th/1.6, height/2])
        ).union(
          this.temple(out_r+th-this.values.sec, th, height, isStruct).rotateZ(90).translate([out_r-th/1.6, (out_r+th-this.values.sec)/2-th*1.5, height/2])
        )
        // // Hooks
        output_structure = output_structure.union(
          this.empty_cyl(hook_d, th, height, isStruct, 15).subtract(
            CSG.cube({radius:[this.values.hook_fence, hook_d, height+2*this.values.e]}).translate([-hook_d/5-this.values.e,-hook_d,-this.values.e])
          ).translate([hook_r+out_r-th*1.2, out_r-this.values.sec+th, 0])
        ).union(
          this.empty_cyl(hook_d, th, height, isStruct, 30).subtract(
            CSG.cube({radius:[hook_d, this.values.hook_fence, height+2*this.values.e]}).translate([-hook_d,-hook_d/5-this.values.e,-this.values.e])
          ).translate([out_r-this.values.sec+th, hook_r+out_r-th*1.2, 0])
        )
        // *****
        //  fixing with or not supports
        // *****
        output_structure = output_structure.union(
          this.fixing(this.values.fix_d, this.values.fix_h+this.values.th, this.values.fix_over, isStruct)
        )
        // translate([out_r-sec+th, hook_r+out_r+th,0])
        // translate([1.5,-2.5,height/2])
        //     rotate([90,0,45])
        //         cylinder(d=th*1.5, h=height/1.6, $fn=6);
        // Better Fixer
        // CSG.cylinder({start: [0, 0, in_d/2-th*2-this.values.e], end: [0, 0, in_d/2+th*2+this.values.e], radius: this.values.holes_d/2, resolution:6}).rotateX(90).rotateZ(90-k*270/(n_holes+2)).translate([out_r-in_r, out_r-in_r, this.values.holes_d])

        output_structure = output_structure.union(
          CSG.cylinder({start: [0,0,0], end:[0,0,this.values.height/1.6], radius:(this.values.th*1.5)/2, resolution:6}).rotateX(90).rotateZ(45).translate([1.5,-2.5,this.values.height/2]).translate([this.values.out_r -this.values.sec + this.values.th, this.values.hook_r+this.values.out_r+this.values.th,0])
        )

        // this.values.arc_l = Math.PI*(out_d+2*th)*3/4;
        return output_structure;
      },
      faceShieldRender(){
        var result = new CSG();
        for(var i=0; i< this.values.n_stacked; i++){
          // console.log(i);
          if(i===0){
            result = result.union(this.mods.simple)
          } else {
            result = result.union(
              this.mods.stacked.translate([0,0,this.values.height*i])
            )
          }
        }
        return result;
      }
    }
  }
  constructMods() {
    return {
      simple : new CSG(),
      stacked : new CSG(),
    }
  }
}
