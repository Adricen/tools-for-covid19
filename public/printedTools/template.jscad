function getParameterDefinitions() {
  // Variable parametres to get an input from user
  return [
    { name: 'diametre', caption: 'Diameter :', type: 'int', default: 95 },
    { name: 'nozzle', caption: 'Nozzle size:', type: 'float', default: 0.4, step: "0.05" },
    { name: 'n_stacked', caption: 'Number of stacked pieces:', type: 'int', default: 3, min: 1 },
    { name: 'fix_n', caption: 'Number of pins:', type: 'int', default: 4,  min: 3 },
    { name: 'thikness', caption: 'General thikness of the headband:', type: 'float', default: 1.2, step: "0.05"  },
    { name: 'pin_sup', type: 'choice', caption: 'Optimized supports:', values: [1, 0], captions: [ "Yes","No"], default: 1},
  ];
}
// Rendered function -> must return a CSG object
// Everything can happen here or inside your object... You choose

// Params coming from getParameterDefinitions function
function main(params) {
  var myBand = new faceShield(params);
  // return a CSG object
  return myBand.modules.faceShieldRender().setColor(1,0.27,0)
}

// Class declaration --> pass the parameters from
class myStuff {
  // Transmit params to constructor
  constructor(parameters) {
    this.values = this.constructValues(parameters)
    this.modules = this.constructModules()
  }
  constructValues(parameters) {
    return {
      val1 : 10,
      val2 : true,
      val3 : {
        subVal1 : 10,
        subVal2 : 203,
      },
      // refer to input value like this
      val4 : parameters.diametre,
      // can have precalculated value depending from others
      get val5() {
        return this.val4/3
      },
    }
  }
  constructModules() {
    var globalParent = this
    return {
      values: globalParent.values,
      empty_cyl(in_d, th, h, dashed=false, fn=60) {
        // Stuff happen
      },
      myOtherStuff(params1, params2) {
        // Some more Stuff
        // Refer other function by using (ex)this.empty_cyl
        // Refer values using this.values
      }
    }
  }
}
