/*
    FILE   : covid19_nose_swab.scad
    AUTHORS :
      - Nicolas H.-P. De Coster (Vigon) <ndcoster@meteo.be>
      - Quentin Bolsée <quentinbolsee@hotmail.com>
      - Antonin R <antoninro@mailfence.com>
      - Adrien Centonze (Adricen) <adrien.centonze@love-open-design.com>
    DATE   : 2020-03-27
    LICENSE : M.I.T. (https://opensource.org/licenses/MIT)
    NOTES  :
      - 0.4 : Openjscad encoding - Adricen
      - 0.3 : size tunings, number of hairs adjusted
      - 0.2 : improved side hairs
      - 0.1 : improved rounded_cube to work with the same parameters as cube()
      - 0.0 : first draft, scraper for testing covid-19 sample.
*/

function getParameterDefinitions() {
  // Variable parametres to get an input from user
  return [
    { name: 'diametre', caption: 'Diameter :', type: 'int', default: 95 },
    { name: 'nozzle', caption: 'Nozzle size:', type: 'float', default: 0.4, step: "0.05" },
    { name: 'n_stacked', caption: 'Number of stacked pieces:', type: 'int', default: 3, min: 1 },
    { name: 'fix_n', caption: 'Number of pins:', type: 'int', default: 4,  min: 3 },
    { name: 'thikness', caption: 'General thikness of the headband:', type: 'float', default: 1.2, step: "0.05"  },
    { name: 'pin_sup', type: 'choice', caption: 'Optimized supports:', values: [1, 0], captions: [ "Yes","No"], default: 1},
  ];
}
// Rendered function -> must return a CSG object
// Everything can happen here or inside your object... You choose

// Params coming from getParameterDefinitions function
function main(params) {
  var mySwab = new swab(params);
  // return a CSG object
  return mySwab.render.setColor(1,0.27,0)
}

// Class declaration --> pass the parameters from
class swab {
  // Transmit params to constructor
  constructor(parameters) {
    this.values = this.constructValues(parameters)
    this.modules = this.constructModules()
    this.render = this.modules.scraper(this.values.scrap_d, this.values.scrap_l, this.values.scrap_h, this.values.hair_r, this.values.dig_central, this.values.dig_n)
  }
  constructValues(parameters) {
    return {
      total_l : 147,

      main_l : 100,
      main_w : 3,
      main_h : 1,
      get main_r() { return this.main_w/3 },

      main_break_dist : 15,

      break_l : 7,
      break_dist : 1,
      break_h : 2.4,
      break_r : 1,
      break_h_bot : 0.6,

      middle_w1 : 1,
      middle_w2 : 0.6,
      middle_h1 : 1,
      middle_h2 : 1.7,

      sec_l : 0,
      sec_w : 1.75,
      sec_h : 0.8,
      get sec_r() { return this.sec_w/3 },

      get third_l() { return this.total_l - (this.sec_l + this.main_l)},
      third_w : 1.75,
      get third_h() { return this.sec_h },

      scrap_d : 1.8*2, //scraper diameter,
      scrap_l : 9,
      scrap_h : 0.8,
      dig_central : 0.6,
      dig_n : 4,
      hair_r : 0.8,
      //main rod
      get shift_1()  { return this.main_l },
      get shift_2()  { return this.shift_1 + this.sec_l },
      get shift_3()  { return this.shift_2 + this.third_l},
    }
  }
  constructModules() {
    var globalParent = this
    return {
      values: globalParent.values,
      scraper(d, l, h, hair_r, central_w, dig_n) {
        var models = {
          get hair_d() { return 2*hair_r },
          eps : 0.001,
          get dig_w() { return (l-d)/(2*dig_n+1) },
          get digCentralHair() {
            var hairOnCenter = new CSG();
            for (var i = 0; i < dig_n+1; i++) {
              hairOnCenter = hairOnCenter.union(
                CSG.cube({radius:[this.dig_w/2, (d+2*hair_r)/2, d/2]}).translate([-(l-d)/2.25+i*2*this.dig_w,0,d/2])
              )
            }
            return hairOnCenter
          },
          get digSideHairs() {
            var hairsOnSide = new CSG();
            for (var i = 0; i < dig_n; i++) {
              hairsOnSide = hairsOnSide.union(
                CSG.cube({radius: [this.dig_w/2, hair_r/2, hair_r/2]}).translate([-(l-d)/2+this.dig_w+i*2*this.dig_w+this.dig_w/2,-d/2-hair_r+hair_r/2,-h-this.eps-hair_r/2])
              ).union(
                CSG.cube({radius: [this.dig_w/2, hair_r/2, hair_r/2]}).translate([-(l-d)/2+this.dig_w+i*2*this.dig_w+this.dig_w/2,d/2+hair_r/2,-h-this.eps-hair_r/2])
              )
            }
            return hairsOnSide
          },
          get mainShape() {
            return CSG.cylinder({start:[0,0,-(l-d)/2], end:[0,0,(l-d)/2], radius:d/2, resolution:30 }).rotateY(90).union(
            CSG.sphere({radius:d/2, resolution:70}).translate([(l-d)/2, 0, 0])
            ).union(
              CSG.sphere({radius:d/2, resolution:70}).translate([-(l-d)/2, 0, 0])
            ).union(
              CSG.cylinder({start:[0,0,-(l-d)/2], end:[0,0,(l-d)/2], radius:this.hair_d/2, resolution:50}).rotateY(90).translate([0, -d/2, -h])
            ).union(
              CSG.cylinder({start:[0,0,-(l-d)/2], end:[0,0,(l-d)/2], radius:this.hair_d/2, resolution:50}).rotateY(90).translate([0, d/2, -h])
            ).subtract(
              // Dig Central
              CSG.cube({radius:[l/2,central_w/2, d/2+2*this.eps]}).translate([0,0,d/2+2*this.eps]).union(
                this.digCentralHair
              )
            ).union(
              this.digSideHairs
            ).union(
              CSG.cube({ radius:[l/2, d/2, h/2], center:[0,0,-h/2], round:true})
            )
          },

        }
        return models.mainShape
        // return models.mainShape
      },
      rounded_cube(size, r=-1,center=false){
        var myCube = {
          eps : 0.01,
          get is_vec(){ return !(size[0] == undefined)},
          get lv() { return is_vec ? size[0] : size },
          get wv() { return is_vec && size.length>=2 ? size[1] : this.lv },
          get lshort() { return Math.min(this.lv, this.wv) },
        }
      }
    }
  }
}
