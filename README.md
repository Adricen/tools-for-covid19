# Coronavirus-production App

The Goal of this repository is to centralize and distribute parametrics tool to fight against covid.

## Contribute

Use the issue tracker to contribute to this eb site. Fork and use pull request so that we can keep track of the progress in real time.

### How can I contribute ?

Fork this repository, clone it localy and start programming your own 3D tool by yourself.

I made a simple Jscad template so you can start with JS oriented object programmation. You'll find this template in `public/printedTools/template.js`

Once you have something promissing, push it up and make me a pull requet so that I can integrate it and share it to everybody !

## Share this site

Hey you have a website of yours? consider embeding this simple site as an article in yours for mass distribution tools and to keep it up to date using thi code :

```html
<iframe id="covidTool" title="Tool for Covid" width="840" height="600"
src="https://adricen.gitlab.io/tools-for-covid19/"></iframe>
```

## More content will follow.

If you have a wweb site and a community, consider embeding it somewhere to make it accessible to the maximum of people.

A wordpress plug-in is on the way :wink:

## License

This web site are under MIT license.

Some of the 3D models provided here are under Creative commons license, some other are under MIT license. Read the code to now more in th printed tool folder.
